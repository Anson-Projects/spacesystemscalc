using Genie
include("lib/deltaV_mass_Isp.jl")

#== server ==#

route("/deltaV") do
  deltaV_mass_Isp.serve()
end



Genie.isrunning(:webserver) || up(9192)