module deltaV_mass_Isp

using Stipple, StippleUI, StipplePlotly
using Random

#== data ==#

g0 = 0.00980665 # km/s^2

function plotdata(model)
    println("plotting")
    fraction = model.initial_mfmp[]:0.01:model.final_mfmp[]
    ΔV = model.Isp[] * g0 * log.(fraction .+ 1)

    model.data[] = [PlotData(
        x=fraction,
        y=ΔV,
    )]
end

#== reactive model ==#

@reactive struct Model <: ReactiveModel
    initial_mfmp::R{Float64} = 0.0
    final_mfmp::R{Float64} = 10.0
    Isp::R{Float64} = 450.0

    process::R{Bool} = false

    data::R{Vector{PlotData}} = []
    layout::PlotLayout = PlotLayout(plot_bgcolor="#fff")
    config::PlotConfig = PlotConfig()
end

function ui(model)
    page(model, [
            heading("ΔV as a function of Mp/Mf and Isp")
            row([
                cell(class="st-module", [
                    h2("Mass Fraction:"),
                    numberfield("Initial Mp/Mf", :initial_mfmp, "filled", @on("keyup", "process = true")),
                    numberfield("Final Mp/Mf", :final_mfmp, "filled", @on("keyup", "process = true")),
                    numberfield("Isp", :Isp, "filled", @on("keyup", "process = true")),
                ])
            ])
            row([
                cell(
                    class="st-module",
                    [
                        plot(:data, style="margin-top: -10pt;")
                    ]
                )
            ])
            row([
                footer([
                    h6("Powered by Stipple")
                ])
            ])], @iif(:isready)
    )
end

function handlers(model)
    on(model.process) do val
        val || return

        if model.initial_mfmp[] <= 0
            model.initial_mfmp[] = 0
        end

        if model.final_mfmp[] <= model.initial_mfmp[]
            model.final_mfmp[] = model.initial_mfmp[] + 10
        end

        if model.Isp[] <= 0
            model.Isp[] = 450
        end



        model.process[] = false
        plotdata(model)
    end

    model
end

function init_plot(model)
    println("serve init")
    plotdata(model)
    model
end

function serve()
    Model |> init |> init_plot |> handlers |> ui |> html
end

end # module